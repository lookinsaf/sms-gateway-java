package sms;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.smslib.*;
import org.smslib.modem.SerialModemGateway;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Logger;



 
 
public class SmsSenderReceiver {
 
    public void sendMessage() throws Exception {
 
        SerialModemGateway gateway = new SerialModemGateway("", "COM11", 9600, "", "");
        gateway.setInbound(true);
        gateway.setOutbound(true);
 
        OutboundNotification outboundNotification = new OutboundNotification();
        InboundNotification inboundNotification = new InboundNotification();
 
        Service service = Service.getInstance();
        service.setOutboundMessageNotification(outboundNotification);
        service.setInboundMessageNotification(inboundNotification);
        service.addGateway(gateway);
        service.startService();
 
//        OutboundMessage msg = new OutboundMessage("0771144240", "one");
//        OutboundMessage msg1 = new OutboundMessage("0773799390", "two");
//        service.sendMessage(msg);
//        service.sendMessage(msg1);
        while (true) {
        	try {
           	 String url = "http://localhost/last.txt";

            	HttpClient client = HttpClientBuilder.create().build();
            	
            	HttpGet request = new HttpGet(url);

            	// add request header
            	//request.addHeader("User-Agent", USER_AGENT);
            	HttpResponse response = client.execute(request);

            	System.out.println("Response Code : " 
                            + response.getStatusLine().getStatusCode());

            	BufferedReader rd = new BufferedReader(
            		new InputStreamReader(response.getEntity().getContent()));
            	System.out.println(response.getEntity().getContent());
            	StringBuffer result = new StringBuffer();
            	String line = "";
            	while ((line = rd.readLine()) != null) {
            		result.append(line);
            	}
            	JSONArray jsonArray = new JSONArray(result.toString());
            	for(int i=0;i<jsonArray.length();i++) {
            		JSONObject co=jsonArray.getJSONObject(i);
            		String mobile=co.get("mobile").toString();
            		String message=co.get("msg").toString();
            		OutboundMessage msg = new OutboundMessage(mobile, message);
            		service.sendMessage(msg);
//            		System.out.println(mobile+":"+message);
            	}
   		} catch (Exception e) {
   			System.out.println(e.getMessage());
   		}
          
		}
        
        
    }
 
    public class InboundNotification implements IInboundMessageNotification {
        @Override
//Get triggered when a SMS is received
        public void process(AGateway gateway, Message.MessageTypes messageTypes, InboundMessage inboundMessage) {
 
            System.out.println(inboundMessage);
            try {
                gateway.deleteMessage(inboundMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
 
    public class OutboundNotification implements IOutboundMessageNotification {
 
//Get triggered when a SMS is sent
        public void process(AGateway gateway, OutboundMessage outboundMessage) {
            System.out.println(outboundMessage);
        }
    }
 
    public static void main(String args[]) {
        SmsSenderReceiver app = new SmsSenderReceiver();
        try {
        	System.out.println("START");
            app.sendMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}