package sms;
import org.smslib.*;
import org.smslib.modem.SerialModemGateway;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class mainsms {

    public void sendMessage() throws Exception {
 
        SerialModemGateway gateway = new SerialModemGateway("", "COM11", 9600, "", "");
        gateway.setInbound(true);
        gateway.setOutbound(true);
 
        OutboundNotification outboundNotification = new OutboundNotification();
        InboundNotification inboundNotification = new InboundNotification();
 
        Service service = Service.getInstance();
        service.setOutboundMessageNotification(outboundNotification);
        service.setInboundMessageNotification(inboundNotification);
        service.addGateway(gateway);
        service.startService();
 
        OutboundMessage msg = new OutboundMessage("0722452122", "test111");
        service.sendMessage(msg);
    }
 
    public class InboundNotification implements IInboundMessageNotification {
        @Override
//Get triggered when a SMS is received
        public void process(AGateway gateway, Message.MessageTypes messageTypes, InboundMessage inboundMessage) {
 
            System.out.println(inboundMessage);
            try {
                gateway.deleteMessage(inboundMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
 
    public class OutboundNotification implements IOutboundMessageNotification {
 
//Get triggered when a SMS is sent
        public void process(AGateway gateway, OutboundMessage outboundMessage) {
            System.out.println(outboundMessage);
        }
    }
 
    public static void main(String args[]) {
        SmsSenderReceiver app = new SmsSenderReceiver();
        try {
            app.sendMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
